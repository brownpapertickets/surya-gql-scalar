const { gql } = require("apollo-server")

export const Scalars = gql`
  scalar Date
`

export const ScalarTypes = {
  String: "String",
  Integer: "Int",
  Float: "Float",
  Boolean: "Boolean",
  ID: "ID",
  Object: "Object",
  ObjectRef: "ObjectRef",
  DateTime: "Date",
  Enum: "Enum",
}
