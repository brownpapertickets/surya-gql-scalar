import { isString } from "util"

import { GraphQLScalarType } from "graphql"
import { Kind } from "graphql/language"

export const DateScalar = {
  Date: new GraphQLScalarType({
    name: "Date",
    description: "Date custom scalar type",
    parseValue(value) {
      // console.log(`Date.parseValue: ${value}`)
      return new Date(value) // value from the client, accepts string or integer format.
    },
    serialize(value) {
      // We need to look closer at how data is stored and formatted. particularly between mongo and pgsql which flows through here.
      if (isString(value)) {
        return value
      } else {
        return value.toISOString()
      }
    },
    parseLiteral(ast) {
      //console.log(`Date.parseLiteral: ${JSON.stringify(ast)}`)
      if (ast.kind === Kind.INT) {
        return new Date(ast.value) // ast value is always in string format ? or int
      } else if (ast.kind === Kind.STRING) {
        return Date.parse(ast.value) // ast value is always in string format
      }
      return null
    },
  }),
}
